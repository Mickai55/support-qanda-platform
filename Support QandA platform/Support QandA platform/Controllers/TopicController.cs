﻿using Support_QandA_platform.DataAccessLayer;
using Support_QandA_platform.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace Support_QandA_platform.Controllers
{
    public class TopicController : Controller
    {
        private DbCtx ctx = new DbCtx();

        // GET: Topics
        public ActionResult Index()
        {
            List<Topic> topics = ctx.Topics.ToList();

            return View(topics);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult topicAdd(string newTopic)
        {
            if (newTopic != "")
                ctx.Topics.Add(new Topic { Name = newTopic, DateCreation = DateTime.Now });
            else
                ctx.Topics.Add(new Topic { Name = "Unnamed Topic", DateCreation = DateTime.Now });
            ctx.SaveChanges();

            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult topicDelete(string topicId)
        {
            List<Topic> topics = ctx.Topics.ToList();
            int numTopicId = Int32.Parse(topicId);

            foreach (Topic topic in topics)
                if (topic.topicId == numTopicId)
                {
                    int count = topic.Subtopics.Count;
                    for (int i = 0; i < count; i++)
                    {
                        this.subtopicDelete(topic.Subtopics.ToList()[0].subtopicId.ToString());
                    }

                    ctx.Entry(topic).State = EntityState.Deleted;

                    ctx.SaveChanges();
                    return RedirectToAction("Index");
                }
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult topicEditNamePage(string topicId)
        {
            List<Topic> topics = ctx.Topics.ToList();
            int numTopicId = Int32.Parse(topicId);

            foreach (Topic topic in topics)
                if (topic.topicId == numTopicId)
                    return View(topic);
            return View(new Topic());
        }

        [Authorize(Roles = "Admin")]
        public ActionResult topicEditName(string topicId, Topic t)
        {
            List<Topic> topics = ctx.Topics.ToList();
            int numTopicId = Int32.Parse(topicId);

            foreach (Topic topic in topics)
                if (topic.topicId == numTopicId)
                    try
                    {
                        if (ModelState.IsValid)
                        {
                            topic.Name = t.Name;

                            ctx.SaveChanges();
                            return RedirectToAction("Index");
                        }
                        else
                            //return RedirectToAction("questionAdd/" + subtopic.subtopicId);
                            return View("topicEditNamePage", t);
                    }
                    catch (Exception e)
                    {
                        return RedirectToAction("Index");
                    }
            return RedirectToAction("Index");
        }


        public ActionResult subtopicSearch(string topicId, string subtopicId)
        {
            List<Topic> topics = ctx.Topics.ToList();
            List<Subtopic> subtopics = ctx.Subtopics.ToList();

            if (subtopicId != null)
            {
                int numId = Int32.Parse(subtopicId);
                int numId2 = Int32.Parse(topicId) - 1;

                foreach (Subtopic subtopic in topics[numId2].Subtopics)
                {
                    if (subtopic.subtopicId == numId)
                    {
                        subtopic.topicID = numId2;
                        return View(subtopic);

                    }
                }
            }

            return View(new Subtopic());
        }

        [Authorize(Roles = "Admin")]
        public ActionResult subtopicAddPage(string topicId)
        {
            List<Topic> topics = ctx.Topics.ToList();
            int numTopicId = Int32.Parse(topicId);

            foreach (Topic topic in topics)
                if (topic.topicId == numTopicId)
                    return View(topic);
            return View(new Topic());
        }

        [Authorize(Roles = "Admin")]
        public ActionResult subtopicAdd(Topic t)
        {
            List<Topic> topics = ctx.Topics.ToList();

            foreach (Topic topic in topics)
            {
                if (t.topicId == topic.topicId)
                {
                    try
                    {
                        topic.Subtopics.Add(new Subtopic { subtopicName = t.newSubtopic, DateCreation = DateTime.Now });
                    }
                    catch (Exception e)
                    {
                        return RedirectToAction("subtopicAddPage", t);
                    }
                }
            }
            ctx.SaveChanges();
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult subtopicDelete(string subtopicId)
        {
            List<Topic> topics = ctx.Topics.ToList();
            int numSubtopicId = Int32.Parse(subtopicId);

            foreach (Topic topic in topics)
                foreach (Subtopic subtopic in topic.Subtopics)
                    if (subtopic.subtopicId == numSubtopicId)
                    {
                        int count = subtopic.Questions.Count;
                        for (int i = 0; i < count; i++)
                        {
                            this.questionDelete(subtopic.Questions.ToList()[0].questionId.ToString());
                        }

                        ctx.Entry(subtopic).State = EntityState.Deleted;

                        ctx.SaveChanges();
                        return RedirectToAction("Index");
                    }
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult subtopicEditNamePage(string subtopicId)
        {
            List<Topic> topics = ctx.Topics.ToList();
            int numSubtopicId = Int32.Parse(subtopicId);

            foreach (Topic topic in topics)
                foreach (Subtopic subtopic in topic.Subtopics)
                    if (subtopic.subtopicId == numSubtopicId)
                    {
                        return View(subtopic);
                    }
            return View(new Subtopic());
        }

        [Authorize(Roles = "Admin")]
        public ActionResult subtopicEditName(string subtopicId, Subtopic s)
        {
            List<Topic> topics = ctx.Topics.ToList();
            int numSubtopicId = Int32.Parse(subtopicId);
            int ss = 0, tt = 0;

            foreach (Topic topic in topics)
                foreach (Subtopic subtopic in topic.Subtopics)
                    if (subtopic.subtopicId == numSubtopicId)
                    {
                        try
                        {
                            if (ModelState.IsValid)
                            {
                                subtopic.subtopicName = s.subtopicName;
                                ss = subtopic.subtopicId;
                                tt = topic.topicId;
                                ctx.SaveChanges();
                                return RedirectToAction("Search/" + tt + "/" + ss);
                            }
                            else
                                return RedirectToAction("subTopicEditNamePage", s);
                        }
                        catch (Exception e)
                        {
                            return RedirectToAction("subTopicEditNamePage", s);
                        }

                    }

            return RedirectToAction("subTopicEditNamePage", s);
        }


        public ActionResult questionSearch(string subtopicId, string questionId)
        {
            List<Topic> topics = ctx.Topics.ToList();

            if (questionId != null)
            {
                int numId = Int32.Parse(questionId);
                int numId2 = Int32.Parse(subtopicId);

                foreach (Topic topic in topics)
                    foreach (Subtopic subtopic in topic.Subtopics)
                    {
                        foreach (Question question in subtopic.Questions)
                        {
                            question.subtopicID = subtopic.subtopicId;

                            if (subtopic.subtopicId == numId2 && question.questionId == numId)
                            {
                                return View(question);
                            }
                        }
                    }
            }

            return View(new Question());
        }

        [Authorize]
        public ActionResult questionAddPage(string subtopicId)
        {
            List<Topic> topics = ctx.Topics.ToList();
            int numSubtopicId = Int32.Parse(subtopicId);

            foreach (Topic topic in topics)
                foreach (Subtopic subtopic in topic.Subtopics)
                    if (subtopic.subtopicId == numSubtopicId)
                        return View(subtopic);
            return View(new Subtopic());
        }

        [Authorize]
        public ActionResult questionAdd(Subtopic s)
        {
            List<Topic> topics = ctx.Topics.ToList();

            foreach (Topic topic in topics)
            {
                foreach (Subtopic subtopic in topic.Subtopics)
                    if (s.topicID == topic.topicId && subtopic.subtopicId == s.subtopicId)
                    {
                        try
                        {
                            if (ModelState.IsValid)
                            {
                                subtopic.Questions.Add(new Question { questionName = s.newQuestion, DateCreation = DateTime.Now });
                            }
                            else
                                //return RedirectToAction("questionAdd/" + subtopic.subtopicId);
                                return View("questionAddPage", s);
                        }
                        catch(Exception e)
                        {
                            return RedirectToAction("questionAdd/" + subtopic.subtopicId);
                        }
                    }
            }
            ctx.SaveChanges();
            return RedirectToAction("Search/" + s.topicID + "/" + s.subtopicId);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult questionEditPage(string questionId)
        {
            List<Topic> topics = ctx.Topics.ToList();
            int numQuestionId = Int32.Parse(questionId);

            foreach (Topic topic in topics)
                foreach (Subtopic subtopic in topic.Subtopics)
                    foreach (Question question in subtopic.Questions)
                            if (question.questionId == numQuestionId)
                                return View(question);

            return View(new Question());
        }

        [Authorize(Roles = "Admin")]
        public ActionResult questionEdit(string questionId, Question q)
        {
            List<Topic> topics = ctx.Topics.ToList();
            int numQuestionId = Int32.Parse(questionId);
            int subtopicId = 0, topicId = 0;

            foreach (Topic topic in topics)
                foreach (Subtopic subtopic in topic.Subtopics)
                    foreach (Question question in subtopic.Questions)
                        if (question.questionId == numQuestionId)
                        {
                            try
                            {
                                if (ModelState.IsValid)
                                {
                                    question.questionName = q.questionName;
                                    subtopicId = subtopic.subtopicId;
                                    topicId = topic.topicId;

                                    ctx.SaveChanges();
                                    return RedirectToAction("Search/" + topicId + "/" + subtopicId);
                                }
                                else
                                    return View("questionEditPage", q);
                            }
                            catch (Exception e)
                            {
                                return View("questionEditPage", q);
                            }
                            
                        }

            return RedirectToAction("Search/" + topicId + "/" + subtopicId);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult questionDelete(string questionId)
        {
            List<Topic> topics = ctx.Topics.ToList();
            int numQuestionId = Int32.Parse(questionId);
            int subtopicId = 0, topicId = 0;

            foreach (Topic topic in topics)
                foreach (Subtopic subtopic in topic.Subtopics)
                    foreach (Question question in subtopic.Questions)
                        if (question.questionId == numQuestionId)
                        {
                            question.Comments.Clear();
                            ctx.Entry(question).State = EntityState.Deleted;

                            //subtopic.Questions.Remove(question);
                            subtopicId = subtopic.subtopicId;
                            topicId = topic.topicId;
                            ctx.SaveChanges();
                            return RedirectToAction("Search/" + topicId + "/" + subtopicId);
                        }
            return RedirectToAction("Search/" + topicId + "/" + subtopicId);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Comment(string subtopicId, string questionId, Question q)
        {
            List<Topic> topics = ctx.Topics.ToList();

            int numId = Int32.Parse(questionId);
            int numId2 = Int32.Parse(subtopicId);

            foreach (Topic topic in topics)
                foreach (Subtopic subtopic in topic.Subtopics)
                    foreach (Question question in subtopic.Questions)
                        if (question.questionId == q.questionId)
                        {
                            question.Comments.Add(new Comment { content = q.newComment, DateCreation = DateTime.Now, author = User.Identity.Name });

                            ctx.SaveChanges();
                            return RedirectToAction("Subtopic/" + subtopicId + "/" + questionId);
                                
                        }
            return RedirectToAction("Subtopic/" + subtopicId + "/" + questionId);
        }

        [Authorize]
        public ActionResult commentEditPage(string commentId)
        {
            List<Topic> topics = ctx.Topics.ToList();
            int numCommentId = Int32.Parse(commentId);

            foreach (Topic topic in topics)
                foreach (Subtopic subtopic in topic.Subtopics)
                    foreach (Question question in subtopic.Questions)
                        foreach (Comment comment in question.Comments)
                            if (comment.commentId == numCommentId)
                                return View(comment);
            
            return View(new Comment());
        }

        [Authorize]
        public ActionResult commentEdit(string commentId, Comment c)
        {
            List<Topic> topics = ctx.Topics.ToList();
            int numCommentId = Int32.Parse(commentId);
            int subtopicId = 0, questionId = 0;

            foreach (Topic topic in topics)
                foreach (Subtopic subtopic in topic.Subtopics)
                    foreach (Question question in subtopic.Questions)
                        foreach (Comment comment in question.Comments)
                            if (comment.commentId == numCommentId)
                            {
                                try
                                {
                                    if (ModelState.IsValid)
                                    {
                                        comment.content = c.content;
                                        subtopicId = subtopic.subtopicId;
                                        questionId = question.questionId;

                                        ctx.SaveChanges();
                                        return RedirectToAction("Subtopic/" + subtopicId + "/" + questionId);
                                    }
                                    else
                                        //return RedirectToAction("questionAdd/" + subtopic.subtopicId);
                                        return View("commentEditPage", c);
                                }
                                catch (Exception e)
                                {
                                    return View("commentEditPage", c);
                                }
                                
                            }

            return RedirectToAction("Subtopic/" + subtopicId + "/" + questionId);
        }

        [Authorize]
        public ActionResult commentDelete(string commentId)
        {
            List<Topic> topics = ctx.Topics.ToList();
            int numCommentId = Int32.Parse(commentId);
            int subtopicId = 0, questionId = 0;

            foreach (Topic topic in topics)
                foreach (Subtopic subtopic in topic.Subtopics)
                    foreach (Question question in subtopic.Questions)
                        foreach (Comment comment in question.Comments)
                            if (comment.commentId == numCommentId)
                            {
                                question.Comments.Remove(comment);
                                subtopicId = subtopic.subtopicId;
                                questionId = question.questionId;
                                ctx.SaveChanges();
                                return RedirectToAction("Subtopic/" + subtopicId + "/" + questionId);
                            }
            return RedirectToAction("Subtopic/" + subtopicId + "/" + questionId);
        }


    }
}