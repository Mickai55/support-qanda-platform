﻿using Support_QandA_platform.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Support_QandA_platform.DataAccessLayer
{
    public class DbCtx : DbContext
    {
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Subtopic> Subtopics { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbCtx() : base("TopicCS")
        {
            Database.SetInitializer<DbCtx>(new Initp());
        }
    }
}