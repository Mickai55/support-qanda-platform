﻿using Support_QandA_platform.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Support_QandA_platform.DataAccessLayer
{
    public class Initp : DropCreateDatabaseAlways<DbCtx>
    {
        protected override void Seed(DbCtx ctx)
        {
            ctx.Topics.Add(new Topic
            {
                Name = "General",
                Subtopics = new List<Subtopic> 
                {
                    new Subtopic 
                    { 
                        subtopicName = "Application start", 
                        Questions = new List<Question> 
                        { 
                            new Question { questionName = "How do I start?", DateCreation = DateTime.Now },
                            new Question { questionName = "How do I go Home?", DateCreation = DateTime.Now },
                            new Question { questionName = "How do I log in?", DateCreation = DateTime.Now }
                        }, 
                        DateCreation = DateTime.Now
                    },
                    new Subtopic 
                    { 
                        subtopicName = "First things to do",
                        Questions = new List<Question>
                        {
                            new Question { questionName = "How do I create a team?", DateCreation = DateTime.Now },
                        },
                        DateCreation = DateTime.Now 
                    }
                },
                DateCreation = DateTime.Now
            });
            ctx.Topics.Add(new Topic
            {
                Name = "Help",
                Subtopics = new List<Subtopic> 
                {
                    new Subtopic { subtopicName = "Creating an account", DateCreation = DateTime.Now },
                    new Subtopic { subtopicName = "Logging in", DateCreation = DateTime.Now },
                    new Subtopic { subtopicName = "My account page", DateCreation = DateTime.Now },
                    new Subtopic { subtopicName = "Account settings", DateCreation = DateTime.Now }
                },
                DateCreation = DateTime.Now
            });
            ctx.Topics.Add(new Topic
            {
                Name = "News",
                Subtopics = new List<Subtopic> 
                {
                    new Subtopic 
                    { 
                        subtopicName = "Groups",
                        Questions = new List<Question>
                        {
                            new Question { questionName = "How do I create a group?", DateCreation = DateTime.Now },
                            new Question { questionName = "How do I add people to my group?", DateCreation = DateTime.Now }
                        },
                        DateCreation = DateTime.Now 
                    },
                    new Subtopic { subtopicName = "Security", DateCreation = DateTime.Now }
                },
                DateCreation = DateTime.Now
            });
            ctx.Topics.Add(new Topic
            {
                Name = "Updates",
                Subtopics = new List<Subtopic>
                {
                    new Subtopic
                    {
                        subtopicName = "Patch 1.05",
                        Questions = new List<Question>
                        {
                            new Question { questionName = "Can I recover my account?", DateCreation = DateTime.Now },
                        },
                        DateCreation = DateTime.Now
                    },
                    new Subtopic { subtopicName = "Patch 1.06", DateCreation = DateTime.Now }
                },
                DateCreation = DateTime.Now
            });
            ctx.SaveChanges();
            base.Seed(ctx);
        }
    }
}