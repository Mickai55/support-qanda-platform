﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Support_QandA_platform.Models
{
    public class Question
    {
        [Key]
        public int questionId { get; set; }
        public int subtopicID { get; set; }

        [Required]
        public String questionName { get; set; }
        public DateTime DateCreation { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }

        public string newComment { get; set; }
    }
}