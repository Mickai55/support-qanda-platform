﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Support_QandA_platform.Models
{
    public class Comment
    {
        [Key]
        public int commentId{ get; set; }

        
        public String content { get; set; }
        public DateTime DateCreation { get; set; }
        public String author { get; set; }
    }
}