﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Support_QandA_platform.Models.Validators;

namespace Support_QandA_platform.Models
{
    public class Subtopic
    {
        [Key]
        public int subtopicId { get; set; }

        public int topicID { get; set; }

        [Required]
        public String subtopicName { get; set; }
        public DateTime DateCreation { get; set; }
        public virtual ICollection<Question> Questions { get; set; }

        [MaxLength(50, ErrorMessage = "The question cannot have than 50 characters!")]
        [QuestionValidator]
        public string newQuestion { get; set; }

    }
}