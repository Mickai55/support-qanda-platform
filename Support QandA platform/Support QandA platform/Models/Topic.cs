﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Support_QandA_platform.Models
{
    public class Topic
    {
        [Key]
        public int topicId { get; set; }
        [Required]
        public String Name { get; set; }
        public DateTime DateCreation { get; set; }
        public virtual ICollection<Subtopic> Subtopics { get; set; }

        [MaxLength(50, ErrorMessage = "The subtopic cannot have than 50 characters!")]
        public string newSubtopic { get; set; }
    }
}