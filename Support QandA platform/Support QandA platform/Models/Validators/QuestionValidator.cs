﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Support_QandA_platform.Models.Validators
{
    public class QuestionValidator : ValidationAttribute 
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var question = (Subtopic)validationContext.ObjectInstance;
            string name = question.newQuestion;

            if (name != null)
                if (name[name.Length - 1] == '?')
                {
                    return ValidationResult.Success;
                }
                else
                {
                    return new ValidationResult("Please enter a question!");
                }
            return ValidationResult.Success;
            
        }

    }
}