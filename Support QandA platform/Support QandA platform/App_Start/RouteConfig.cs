﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Support_QandA_platform
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "subtopicID",
                url: "{controller}/Subtopic/{subtopicId}/{questionId}",
                defaults: new { controller = "Topic", action = "questionSearch" }
            );

            routes.MapRoute(
                name: "topicID",
                url: "{controller}/Search/{topicId}/{subtopicID}",
                defaults: new { controller = "Topic", action = "subtopicSearch" }
            );

            routes.MapRoute(
                name: "addQuestionPage",
                url: "{controller}/questionAdd/{subtopicId}",
                defaults: new { controller = "Topic", action = "questionAddPage" }
            );

            routes.MapRoute(
                name: "addSubtopicPage",
                url: "{controller}/AddNewSubtopic/{topicId}",
                defaults: new { controller = "Topic", action = "subtopicAddPage" }
            );

            routes.MapRoute(
                name: "commentEditPage",
                url: "{controller}/EditComment/{commentId}",
                defaults: new { controller = "Topic", action = "commentEditPage" }
            );

            routes.MapRoute(
                name: "questionEditPage",
                url: "{controller}/EditQuestion/{questionId}",
                defaults: new { controller = "Topic", action = "questionEditPage" }
            );

            routes.MapRoute(
                name: "topicEditNamePage",
                url: "{controller}/EditNameTopic/{topicId}",
                defaults: new { controller = "Topic", action = "topicEditNamePage" }
            );

            routes.MapRoute(
                name: "subtopicEditNamePage",
                url: "{controller}/EditNameSubtopic/{subtopicId}",
                defaults: new { controller = "Topic", action = "subtopicEditNamePage" }
            );

            routes.MapRoute(
                name: "addQuestion",
                url: "{controller}/{action}/{subtopicId}/{topicId}/{s}",
                defaults: new { controller = "Topic", action = "questionAdd" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
